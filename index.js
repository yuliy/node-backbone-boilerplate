/**
 * Module dependencies.
 */
var path           = require('path');
var express        = require('express');
var bodyParser     = require('body-parser');
var morgan         = require('morgan');
var methodOverride = require('method-override');
var errorHandler   = require('errorhandler');

/**
 * Local Dependencies
 */
var config = require('./config');
var api    = require('./routes/api');

/**
 * Initialize the Express App
 */
var app = express();

/**
 * General Setup for the Express App
 */
app.set('port', process.env.PORT || config.port);           // set the app port
app.use(morgan('dev'));                                     // setup logging in dev mode
app.use(bodyParser.json());                                 // setup middleware to parse JSON
app.use(methodOverride());                                  // setup middleware suppot for PUT and DELETE HTTP verbs
app.use(express.static(path.join(__dirname, 'client')));    // setup support for serving static content (HTML/JS/CSS)

/**
 * Development Mode only Setup for the Express App
 */
if ('development' == app.get('env'))
{
    app.use(errorHandler());
}

/**
 * Add Routes to the Express App.  In this boilerplate we use the Express App only as an API endpoint so we will add API routes here.
 */
app.get("/api/models", api.models);

/**
 * Start the Express App
 */
app.listen(app.get('port'), function() {
	console.log('Express server listening on port ' + app.get('port'));
});
