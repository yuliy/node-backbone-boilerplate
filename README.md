node-backbone-boilerplate
----
### About
A boilerplate for starting Node.js/Backbone applications.  Includes a gulp process for bundling JS and compiling LESS files.

### Install
* Run `npm install` in the top level directory to setup all Node.js dependencies
* Run `npm install` in the `client` directory to setup all the gulp build dependencies
* Run `gulp build --gulpfile client/gulp.js` in the top level directory to build JS/CSS

### Development
* Run `gulp watch-nosync --gulpfile client/gulp.js` in the top level directory to start a build/watch chain.  During this time you can make edits to JS, HBS or LESS files and the build process will rerun appropriately.
* Run `gulp watch-sync --gulpfile client/gulp.js` in the top level directory to start a build/watch chain.  During this time you can make edits to JS, HBS or LESS files and the build process will rerun appropriately and the browser will reload.

### Usage
* Run `node index.js` in the top level directory to start the node application