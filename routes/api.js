function models(req, res) {
    res.send(
        [
            {
                'id': 1,
                'name': 'model1'
            },
            {
                'id': 2,
                'name': 'model2'
            }
        ]
    );
}

exports.models = models;