/**
 * Primary Dependencies
 */
var Backbone = require('backbone');
var $        = require('jquery');
Backbone.$ = $;

/**
 * Local Dependencies
 */
var template = require("../templates/default.hbs");

var DefaultView = Backbone.View.extend({
    template: template,

    /**
     * Rendering function for the About View.
     */
    render: function() {
        this.$el.html(this.template({}));
        return this;
    }
});

module.exports = DefaultView;