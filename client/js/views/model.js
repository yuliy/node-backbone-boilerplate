/**
 * Primary Dependencies
 */
var Backbone = require('backbone');
var $        = require('jquery');
Backbone.$ = $;

/**
 * Local Dependencies
 */
var template = require("../templates/model.hbs");

var ModelView = Backbone.View.extend({
    tagName: 'li',
    template: template,

    /**
     * Rendering function for rendering a Band Model, done by simply passing the model attributes to
     * the Band Template.
     */
    render: function() {
        this.$el.html(this.template(this.model.attributes));
        return this;
    }
});
module.exports = ModelView;