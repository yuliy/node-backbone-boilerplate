/**
 * Primary Dependencies
 */
var Backbone = require('backbone');
var $        = require('jquery');
Backbone.$ = $;

/**
 * Local Dependencies
 */
var template = require("../templates/header.hbs");

var HeaderView = Backbone.View.extend({
    template: template,

    /**
     * Rendering function for the Header View.
     */
    render: function() {
        this.$el.html(this.template());
        return this;
    }
});

module.exports = HeaderView;