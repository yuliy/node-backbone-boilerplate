/**
 * Primary Dependencies
 */
var Backbone = require('backbone');
var $        = require('jquery');
Backbone.$ = $;

/**
 * Local Dependencies
 */
var Collection = require("../collections/collection");
var ModelView = require("../views/model");
var template = require("../templates/collection.hbs");

var CollectionView = Backbone.View.extend({
    template: template,

    /**
     * Initialize this view by loading a new Collection.
     */
    initialize: function() {
        this.collection = new Collection();

        // pass through the username to be used as a parameter in the API call
        this.collection.fetch(
            {
                reset: true
            }
        );

        this.listenTo(this.collection, 'reset', this.render);
    },

    /**
     * Rendering function for displaying the following collection.  We will iterate over all the bands
     * in the collection and render each one individually.
     */
    render: function() {
        // render the template for this element
        this.$el.html(
            this.template(
                {}
            )
        );

        // iterate over the collection and render model each individually
        this.collection.each(function(item) {
            this.renderModel(item);
        }, this );

        return this;
    },

    /**
     * Render each Model individually by generating a new Model View and inserting it into the collection list
     * element in the View.
     *
     * @param {Backbone.Model} item The Model to be rendered
     */
    renderModel: function(item) {
        var modelView = new ModelView({
            model: item
        });
        this.$el.find('#collection_list ul').append( modelView.render().el );
    }
});
module.exports = CollectionView;