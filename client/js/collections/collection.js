/**
 * Primary Dependencies
 */
var Backbone = require('backbone');
var $        = require('jquery');
Backbone.$ = $;

/**
 * Local Dependencies
 */
var Model = require('../models/model');

var Collection = Backbone.Collection.extend({
	model: Model,
	url: '/api/models'
});

module.exports = Collection;