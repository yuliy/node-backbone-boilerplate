/**
 * Primary Dependencies
 */
var Backbone = require('backbone');
var $        = require('jquery');
Backbone.$ = $;

/**
 * Local Dependencies
 */
var HeaderView     = require('./views/header');
var CollectionView = require('./views/collection');
var AboutView      = require('./views/about');
var DefaultView    = require('./views/default');

/**
 * Override the default Backbone.View.close function to include a console log about the view getting closed and add
 * support for a before close callback.  This is taken from https://github.com/ccoenraets/backbone-cellar a tutorial
 * for writing backbone applications.
 */
Backbone.View.prototype.close = function () {
    console.log('Closing view ' + this);
    if (this.beforeClose) {
        this.beforeClose();
    }
    this.remove();
    this.unbind();
};

var AppRouter = Backbone.Router.extend({
    routes: {
        "": "getHome",
        "about": "getAbout",
        "*default": "getDefault"
    },

    /**
     * Initialize the App Router
     */
    initialize: function() {
        var header = new HeaderView();
        $("#header").html(header.render().el);
    },

    /**
     * Utility function to switch the current view to a new view.  To do so we must close the current view and
     * render the new view into some selector on the page.
     *
     * Some logic used here is based on the work from https://github.com/ccoenraets/backbone-cellar a tutorial for
     * writing backbone applications.
     *
     * @param {string}        selector The string representing the jQuery selector inside of which we are updating the view
     * @param {string}        viewType The string representing the type of the view we are switching to
     * @param {Backbone.View} view     The new view object we are switching to
     * @param {bool}          initial  This is a flag determining if this is the first view getting populated by the router
     */
    _switchView: function(selector, viewType, view, initial) {
    	// if this is not the first view starting the app then we must close the current view
        if (!initial) {
            this.currentView.close();
        }

        $(selector).html(view.render().el);
        $(selector).data('view-type', viewType);
        this.currentView = view;
    },

    /**
     * Utility function to transition from the current view into a new view.
     *
     * @param {string}        selector The string representing the jQuery selector inside of which we are updating the view
     * @param {string}        viewType The string representing the type of the view we are transitioning to
     * @param {Backbone.View} view     The new view object we are transitioning to
     */
    _transition: function (selector, viewType, view) {
        var that = this;

        // if we have a view currently then we need to initiate a fade out
        if (this.currentView) {
            $(selector).hide
            (
                400,
                // upon finishing the fade out of the current view we must switch the view and fade in
                function() {
                    that._switchView(selector, viewType, view, false)
                    $(selector).show(400);   
                }
            );
        } else {
            // if we did not have a view yet (page has just loaded) then we can just render it normally
            this._switchView(selector, viewType, view, true);
        }
    },

    /**
     * Route function for retrieving the Home View.
     */
    getHome: function() {
        this._transition('#content', 'home', new CollectionView());
    },

    /**
     * Route function for retrieving the About View.
     */
    getAbout: function() {
        this._transition('#content', 'about', new AboutView());
    },

    /**
     * Route function for retrieving the 404 View.
     */
    getDefault: function() {
        this._transition('#content', 'default', new DefaultView());
    }
});

module.exports = AppRouter;