/**
 * Primary Dependencies
 */
var Backbone = require('backbone');
var $        = require('jquery');
Backbone.$ = $;

var Model = Backbone.Model.extend();
module.exports = Model