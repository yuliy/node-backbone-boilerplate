/**
 * Primary Dependencies
 */
var Backbone = require('backbone');
var $        = require('jquery');
Backbone.$ = $;

/**
 * Local Dependencies
 */
var AppRouter = require('./router');

// Instantiate the router
var app_router = new AppRouter;

// Start Backbone history a necessary step for bookmarkable URL's
Backbone.history.start();