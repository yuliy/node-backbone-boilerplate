node-backbone-boilerplate client
----
### About
Client application for the Node/js/Backbone boilerplate application.

### Install
* Run `npm install` in this directory to setup all the gulp build dependencies
* Run `gulp build --gulpfile gulp.js` in this directory to build JS/CSS

### Development
* Run `gulp watch-nosync --gulpfile gulp.js` in the top level directory to start a build/watch chain.  During this time you can make edits to JS, HBS or LESS files and the build process will rerun appropriately.
* Run `gulp watch-sync --gulpfile gulp.js` in the top level directory to start a build/watch chain.  During this time you can make edits to JS, HBS or LESS files and the build process will rerun appropriately and the browser will reload.