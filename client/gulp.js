var path         = require('path');
var gulp         = require('gulp');
var less         = require('gulp-less');
var gutil        = require('gulp-util');
var source       = require('vinyl-source-stream');
var browserify   = require('browserify');
var browserSync  = require("browser-sync");
var prettyHrtime = require('pretty-hrtime');
var config       = require('../config');

/**
 * Flag to determine if we are going to reload the browser after rebundling or recompiling CSS
 */
var reload = false;

/**
 * The 'browser-sync' gulp task will initialize Browser Sync using a proxy for where the Node
 * app is running (localhost:{config.port})
 */
gulp.task('browser-sync', function() {
    browserSync({
        proxy: 'localhost:' + config.port
    });
});

/**
 * The 'compile-js' gulp task will rebundle the primary JS file into a bundle for use on the
 * frontend.  If the "reload" flag is set to true then it will initiate a browser sync reload.
 */
gulp.task('compile-js', function() {
    var start = process.hrtime();

    browserify('./js/app.js')
        .bundle()
        //Pass desired output filename to vinyl-source-stream
        .pipe(source('bundle.js'))
        // Start piping stream to tasks!
        .pipe(gulp.dest('./dist/js/'))
        .on('end', function() {
            var end = process.hrtime(start);

            gutil.log(
                "Done bundling " + 
                gutil.colors.green("bundle.js") + 
                " in " + 
                gutil.colors.magenta(prettyHrtime(end))
            );

            if (reload) {
                browserSync.reload();
            }
        });
});

/**
 * The 'compile-less' gulp task will recompile the primary LESS file into a CSS file.  If
 * the "reload" flag is set to true then it will initiate a browser sync reload.
 */
gulp.task('compile-less', function () {
    var start = process.hrtime();

      gulp.src('./less/style.less')
        .pipe(less({
              paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./dist/css/'))
        .on('end', function() {
            var end = process.hrtime(start);

            gutil.log(
                "Done compiling " + 
                gutil.colors.green("style.css") + 
                " in " + 
                gutil.colors.magenta(prettyHrtime(end))
            );

            if (reload) {
                browserSync.reload();
            }
        });
});

/**
 * The 'build' gulp task will:
 *
 * 1. Rebundle JS ('browserify')
 * 2. Recompile LESS ('compile-less')
 * 3. Notify completion
 */
gulp.task('build', ['compile-js', 'compile-less'], function() {
    gutil.log("Finished building JS and CSS.");
});

/**
 * The 'watch' gulp task will initialize the watching of JS, Handlebars, and LESS files and
 * appropriately rebundle or recompile when it detects a change in any of them.
 */
gulp.task('watch', function() {
    // watch JS files for changes and rebundle
    gulp.watch("./js/**/*.js", ['compile-js']);

    // watch Handlebars files for changes and rebundle
    gulp.watch("./js/**/*.hbs", ['compile-js']);

    // watch LESS files for changes and recompile
    gulp.watch('./less/**/*.less', ['compile-less']);

    gutil.log("Watch started...");
});

/**
 * The 'watch-nosync' gulp task will:
 *
 * 1. Build JS and CSS ('build')
 * 2. Initiate watch of JS, Handlebars Templates and LESS files ('watch')
 */
gulp.task('watch-nosync', ['build', 'watch'], function() {});

/**
 * The 'watch-sync' gulp task will:
 *
 * 1. Initialize Browser Sync
 * 2. Build JS and CSS ('build')
 * 3. Initiate watch of JS, Handlebars Templates and LESS files ('watch')
 * 4. Enable browser reloading
 */
gulp.task('watch-sync', ['browser-sync', 'build', 'watch'], function() {
    // set the reload flag to true so that we will reload browsers
    reload = true;
});